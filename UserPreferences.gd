extends Node

signal changed(section, key, val)

const CONFIG_PATH := "user://settings.cfg"
const SETTINGS := {
	gfx = {
		brightness = { default = 1.0, bound = [0.5, 1.5] },
		motion_blur = { default = true },
		glow = { default = true },
		ss_reflections = { default = true },
		fullscreen = { default = false },
		resolution = {
			default = "1920x1080",
			choices = [
				"1920x1080",
				"3840x2160",
				"2560x1440",
				"1600x900",
				"1280x1024",
				"1280x800",
				"1280x720",
				"1152x864",
				"1024x768",
				"800x600",
				"720x480",
				"640x480",
			]
		},
	},
	sfx = {
		master_volume = { default = 0.0, bound = [-24, 6] },
		sound_volume = { default = 0.0, bound = [-24, 6] },
		music_volume = { default = 0.0, bound = [-24, 6] },
	},
}

var config := ConfigFile.new()
var default_env := preload("res://default_env.tres")

func normalize(section: String, key: String, val):
	var info = SETTINGS.get(section, {}).get(key, {})
	if "bound" in info:
		return clamp(val, info.bound[0], info.bound[1])
	if "options" in info and not info.options.has(val):
		printerr("%s.%s=%s not in %s" % [section, key, val, info.options.join(",")])
		return info.options[0]
	return val

func _ready():
	config.load(CONFIG_PATH)
	for section in config.get_sections():
		for key in config.get_section_keys(section):
			if not SETTINGS.get(section, {}).get(key):
				printerr("Config file contains unknown key %s.%s" % [section, key])
				continue
			handle_change(section, key, get_value(section, key))

func get_value(section: String, key: String):
	var info = SETTINGS[section][key]
	var val = config.get_value(section, key, info.default)
	val = normalize(section, key, val)
	return val

func set_value(section: String, key: String, val):
	val = normalize(section, key, val)
	config.set_value(section, key, val)
	config.save(CONFIG_PATH)
	handle_change(section, key, val)

func handle_change(section: String, key: String, val):
	emit_signal("changed", section, key, val)
	match section:
		"gfx":
			match key:
				"motion_blur":
					pass  # handled in motion_blur.gd
				"glow":
					default_env.glow_enabled = val
				"brightness":
					default_env.adjustment_brightness = val
				"ss_reflections":
					default_env.ss_reflections_enabled = val
				"fullscreen":
					OS.set_window_fullscreen(val)
				"resolution":
					return
				_:
					printerr("Unknown config: %s.%s" % [section, key])
		"sfx":
			match key:
				"master_volume":
					var idx := AudioServer.get_bus_index("Master")
					AudioServer.set_bus_volume_db(idx, val)
				"sound_volume":
					var idx := AudioServer.get_bus_index("Sound")
					AudioServer.set_bus_volume_db(idx, val)
				"music_volume":
					var idx := AudioServer.get_bus_index("Music")
					AudioServer.set_bus_volume_db(idx, val)
				_:
					printerr("Unknown config: %s.%s" % [section, key])
		_:
			printerr("Unknown config: %s.%s" % [section, key])

func gfx_configured() -> bool:
	# If any of these are set, we've already detected gfx settings
	# Otherwise, we'll try to detect during the intro stage
	return (
		config.has_section_key("gfx", "motion_blur") or
		config.has_section_key("gfx", "glow") or
		config.has_section_key("gfx", "ss_reflections")
	)
