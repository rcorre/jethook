extends KinematicBody
class_name Ghost

const GRAVITY := 9.8
const DAMPING := 0.2
const JET_SPEED := 10.0
const JET_SLOW_FACTOR := 0.1

signal name_set(name)
signal time_set(timestr)
signal grapple_on(pos)
signal grapple_off()
# signal jet(dir) TODO

var lap_number := 0
var start_frame := 0
var idx := 0
var grapple_list: PoolVector3Array
var frame_list: PoolIntArray
var expected_time := 0.0

var linear_velocity := Vector3.ZERO
var cur_grapple := InputRecord.GRAPPLE_OFF
var tween := Tween.new()

func start(record: InputRecord.Data):
	frame_list = record.frames
	grapple_list = record.grapple
	expected_time = record.time
	emit_signal("name_set", record.name)
	emit_signal("time_set", "%4.2f\n" % [record.time])

	add_child(tween)
	for i in range(1, len(record.frames)):
		var p1 := record.pos[i - 1]
		var p2 := record.pos[i]
		var t1 := float(record.frames[i - 1]) / Engine.iterations_per_second
		var t2 := float(record.frames[i]) / Engine.iterations_per_second

		var duration := t2 - t1
		var delay := t1
		tween.interpolate_property(
			self,
			"global_transform:origin",
			p1,
			p2,
			duration,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN_OUT,
			delay
		)
		tween.start()

func _physics_process(_delta: float):
	if start_frame == 0:
		start_frame = get_tree().get_frame()

	if idx < frame_list.size() and frame_list[idx] == get_tree().get_frame() - start_frame:
		var grapple := grapple_list[idx]
		if grapple != cur_grapple:
			cur_grapple = grapple
			if grapple == InputRecord.GRAPPLE_OFF:
				emit_signal("grapple_off")
			else:
				emit_signal("grapple_on", grapple)
		idx += 1

func lap_finished(max_laps: int):
	lap_number += 1
	if lap_number > max_laps:
		var frames := get_tree().get_frame() - start_frame
		var time := float(frames) / Engine.iterations_per_second
		if not is_equal_approx(time, expected_time):
			push_warning("Ghost time expected=%.2f, actual=%.2f" % [expected_time, time])
		else:
			print_debug("Lap time ok. %.2f=%.2f" % [expected_time, time])
		queue_free()
