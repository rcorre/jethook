extends Spatial

onready var pivot_x := $CameraPivotX as Spatial

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _on_look(dir: Vector2):
	rotation.y = dir.y
	pivot_x.rotation.x = dir.x
