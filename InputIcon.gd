tool
extends Control

onready var key_label: Label = $KeyLabel
onready var left_click: Control = $LeftClick
onready var right_click: Control = $RightClick

var action: String

func _ready():
	# TODO: detect controller
	for c in get_children():
		c.hide()
	var ev: InputEvent = ProjectSettings.get("input/" + action).events[0]
	var mb := ev as InputEventMouseButton 
	if mb and mb.button_index == BUTTON_LEFT:
		left_click.show()
	elif mb and mb.button_index == BUTTON_RIGHT:
		right_click.show()
	else:
		key_label.show()
		key_label.set_text(ev.as_text())

func _get_property_list() -> Array:
	var hint := ""
	for p in ProjectSettings.get_property_list():
		if p.name.find("input/") == 0:
			hint += p.name.trim_prefix("input/") + ","

	return [{
		"name": "action",
		"type": TYPE_STRING,
		"hint": PROPERTY_HINT_ENUM,
		"hint_string": hint.trim_suffix(","),
	}]
