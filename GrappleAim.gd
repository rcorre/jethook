extends RayCast

signal hook(global_pos)
signal release

export(NodePath) var holder_path: NodePath

onready var indicator := $Indicator as Spatial
onready var anim := $AnimationPlayer as AnimationPlayer

var on := false  # button pressed, hook when our raycast hist
var hooked := false  # the hook is attached

func _physics_process(_delta: float):
	indicator.set_visible(not hooked)

	if is_colliding():
		if on and not hooked:
			hooked = true
			emit_signal("hook", get_collision_point())
		indicator.global_transform.origin = get_collision_point()
		indicator.global_transform.basis.z = get_collision_normal()
		anim.play("GrappleInRange")
	else:
		indicator.transform = Transform.IDENTITY.translated(cast_to / 4)
		anim.play("GrappleOutOfRange")

func _on_grapple(grapple_on: bool):
	on = grapple_on
	if not on:
		emit_signal("release")
		hooked = false
