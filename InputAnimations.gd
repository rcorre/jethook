extends AnimationPlayer

func _ready():
	Event.connect("race_intro_started", self, "play_backwards", ["Fade"])
	Event.connect("race_countdown_started", self, "play", ["Fade"])
