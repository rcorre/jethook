extends Area

export(float, 0, 10000, 1) var impulse := 50

onready var sound: AudioStreamPlayer3D = $Sound

func _ready():
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body: Spatial):
	body.linear_velocity += -global_transform.basis.z * impulse
	sound.play()
