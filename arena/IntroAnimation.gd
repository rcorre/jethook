extends AnimationPlayer

export(float, 0, 20) var start_time := 5.0

func _ready():
	Event.emit_signal("race_intro_started")
	if not UserPreferences.gfx_configured():
		call_deferred("_configure_gfx")

func _configure_gfx():
	var timer := Timer.new()
	add_child(timer)
	timer.process_mode = Timer.TIMER_PROCESS_PHYSICS

	# skip the first second, for framerate to stabilize
	timer.start(1.0)
	yield(timer, "timeout")
	var start := Engine.get_frames_drawn()
	var cur := start

	# Now check back each second
	# If the framerate is low, reduce settings
	timer.start(1.0)
	yield(timer, "timeout")
	if Engine.get_frames_drawn() - start < 50: 
		print_debug("Disabling ss_reflections for performance")
		UserPreferences.set_value("gfx", "ss_reflections", false)
	else: 
		UserPreferences.set_value("gfx", "ss_reflections", true)
		return
	start = Engine.get_frames_drawn()

	timer.start(1.0)
	yield(timer, "timeout")
	if Engine.get_frames_drawn() - start < 50: 
		print_debug("Disabling motion blur for performance")
		UserPreferences.set_value("gfx", "motion_blur", false)
	else: 
		UserPreferences.set_value("gfx", "motion_blur", true)
		return
	start = Engine.get_frames_drawn()

	timer.start(1.0)
	yield(timer, "timeout")
	if Engine.get_frames_drawn() - start < 50: 
		print_debug("Disabling glow for performance")
		UserPreferences.set_value("gfx", "glow", false)
	else: 
		UserPreferences.set_value("gfx", "glow", true)
		return

func _input(ev: InputEvent):
	# allow the user to skip the intro only if we've already run a graphics test
	if (
		ev.is_action_pressed("ui_accept")
		and current_animation_position < start_time 
		and UserPreferences.gfx_configured()
	):
		seek(start_time)
		Event.emit_signal("quick_fade")

func start_countdown():
	Event.emit_signal("race_countdown_started")

func start_race():
	Event.emit_signal("race_started")
