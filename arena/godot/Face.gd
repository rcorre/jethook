extends MeshInstance

export(Material) var normal_material: Material
export(Material) var grabbed_material: Material
export(Material) var hit_material: Material

var grabbed := false
var hit := false

func _on_grappled(on: bool):
	grabbed = on
	pick_material()

func _on_player_collided():
	# briefly show a frown, then reset
	hit = true
	pick_material()
	yield(get_tree().create_timer(1.0), "timeout")
	hit = false
	pick_material()

func pick_material():
	var mat := normal_material
	if hit:
		mat = hit_material
	elif grabbed:
		mat = grabbed_material
	set_surface_material(1, mat)
