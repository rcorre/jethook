extends Spatial

var tween := Tween.new()

func _ready():
	add_child(tween)
	tween.repeat = true
	var delay := randf()
	tween.interpolate_property(self, "global_transform", global_transform, global_transform.translated(Vector3.UP), 0.5, Tween.TRANS_QUAD, Tween.EASE_IN, delay)
	tween.interpolate_property(self, "global_transform", global_transform.translated(Vector3.UP), global_transform, 0.5, Tween.TRANS_QUAD, Tween.EASE_OUT, delay + 0.5)
	tween.start()
