extends Label

func _on_countdown_changed(val: int):
	match val:
		3: set_text("Hook")
		2: set_text("Jet")
		1: set_text("Ready")
		0: set_text("Go!")

func _on_lap_times_changed(times: PoolRealArray, max_laps: int):
	var new_text := "Lap %d/%d\n" % [len(times), max_laps]
	for i in len(times):
		var t := times[i]
		new_text += "%4.2f\n" % [t]
	text = new_text
