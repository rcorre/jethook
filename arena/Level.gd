extends Node
class_name Level

export(int) var level_id: int

func _enter_tree():
	add_to_group("level")

func _ready():
	var players := Spatial.new()
	players.name = "Players"
	add_child(players)

	var spawns := get_tree().get_nodes_in_group("spawn")

	var player_scene := preload("res://PlayerLocal.tscn")
	if get_tree().network_peer == null:
		var player := player_scene.instance() as Spatial
		add_child(player)
		player.global_transform = spawns[0].global_transform
		return

	# multiplayer setup
	var peers := Array(get_tree().get_network_connected_peers())
	peers.append(get_tree().get_network_unique_id())
	peers.sort()  # for consistent spawn locations across peers

	assert(len(spawns) >= len(peers))

	for i in len(peers):
		var player := player_scene.instance() as Spatial
		player.name = String(peers[i])
		players.add_child(player)
		player.global_transform = spawns[i].global_transform

func start():
	get_tree().call_group("player", "activate")
	if get_tree().network_peer == null:
		_time_trial_start()

func _time_trial_start():
	# singleplayer setup
	var ghost_data := InputRecord.load(level_id)
	var start_trans = get_tree().get_nodes_in_group("player")[0].global_transform
	for data in ghost_data:
		if data.level != level_id:
			return
		var ghost := preload("res://PlayerGhost.tscn").instance() as Ghost
		get_tree().current_scene.add_child(ghost)
		ghost.global_transform = start_trans
		ghost.start(data)
