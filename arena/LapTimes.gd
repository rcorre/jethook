extends GridContainer

func _ready():
	Event.connect("lap_completed", self, "_on_lap_completed")
	Event.connect("time_trial_finished", self, "_on_time_trial_finished")

func add_label(text: String):
	var label := Label.new()
	label.align = label.ALIGN_CENTER
	label.set_text(text)
	add_child(label)

func _on_lap_completed(lapnum: int, time: float):
	add_label("Lap %d" % lapnum)
	add_label("%4.2f" % time)

func _on_time_trial_finished():
	add_child(HSeparator.new())
	add_child(HSeparator.new())
	add_label("Total")
	add_label("%4.2f" % Global.player().recorder.data.time)
