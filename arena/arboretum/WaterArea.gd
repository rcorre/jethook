extends Area

func _ready():
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")

func _on_body_entered(body: Node):
	body.propagate_call("_on_water_entered", [true])
	var idx := AudioServer.get_bus_index("UnderWater")
	AudioServer.set_bus_bypass_effects(idx, false)

func _on_body_exited(body: Node):
	body.propagate_call("_on_water_entered", [false])
	var idx := AudioServer.get_bus_index("UnderWater")
	AudioServer.set_bus_bypass_effects(idx, true)
