extends Control

func _ready():
	Event.connect("time_trial_finished", self, "queue_free")

func pause():
	var idx := AudioServer.get_bus_index("Dull")
	AudioServer.set_bus_bypass_effects(idx, false)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().paused = true
	show()

func unpause():
	var idx := AudioServer.get_bus_index("Dull")
	AudioServer.set_bus_bypass_effects(idx, true)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().paused = false
	hide()

func _input(ev: InputEvent):
	if ev.is_action_pressed("ui_cancel"):
		if get_tree().paused:
			unpause()
		else:
			pause()
