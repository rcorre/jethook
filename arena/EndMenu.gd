extends Control

func _ready():
	Event.connect("time_trial_finished", self, "_on_time_trial_finished")

func _on_time_trial_finished():
	show()
	var idx := AudioServer.get_bus_index("Dull")
	AudioServer.set_bus_bypass_effects(idx, false)
