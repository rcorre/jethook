extends Area
class_name FinishLine

signal crossed

export(int) var max_laps := 3

onready var anim_player: AnimationPlayer = $AnimationPlayer

var countdown_left := 3
var lap_number := 0
var lap_times := PoolRealArray()
var level_name: String

func _ready():
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body: Node):
	var ghost := body as Ghost
	if ghost:
		ghost.lap_finished(max_laps)
		return

	emit_signal("crossed")
	if lap_number > 0:
		Event.emit_signal("lap_completed", lap_number, lap_times[lap_number - 1])
	lap_number += 1
	lap_times.append(0)

	if lap_number > max_laps:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		Event.emit_signal("time_trial_finished")

func _physics_process(delta: float):
	if lap_number > 0 and lap_number <= max_laps:
		lap_times[lap_number - 1] += delta
		propagate_call("_on_lap_times_changed", [lap_times, max_laps])

func countdown():
	if countdown_left == 3:
		var players = get_tree().get_nodes_in_group("player")
		for p in players:
			if p.is_local():
				p.focus_camera()
	propagate_call("_on_countdown_changed", [countdown_left])
	countdown_left -= 1
	anim_player.play("SetText")
