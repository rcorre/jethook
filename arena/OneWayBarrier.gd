extends Area

export(float, 0, 100, 1) var force := 10.0

func _physics_process(delta: float):
	var allowed_direction := global_transform.basis.z.normalized()
	for body in get_overlapping_bodies():
		var r: Player = body
		if sign(r.linear_velocity.z) != sign(allowed_direction.z):
			r.linear_velocity += allowed_direction * force * delta * r.linear_velocity.length()
