extends BaseButton

# Must be a path rather than a PackedScene to avoid a cyclic import
export(String, FILE, "*.tscn") var scene_path: String

func _pressed():
	get_tree().change_scene(scene_path)
