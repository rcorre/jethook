extends Node

const DEFAULT_PORT = 7472

signal host_ok(address)
signal host_error(msg)

const _ERROR_MSG = {
	ERR_ALREADY_IN_USE : "Already in use",
	ERR_CANT_CREATE    : "Can't create",
}

func _fail(err: int):
	var msg: String = _ERROR_MSG.get(err, "Unknown error %d" % err)
	emit_signal("host_error", msg)
	printerr("host_error: ", msg)

func _ready():
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("network_peer_disconnected", self, "_on_network_peer_disconnected")
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

func _on_connected_to_server():
	# TODO: client emits this, just tell server you are ready and let it initiate
	rpc("begin")

func _on_connection_failed():
	pass

func _on_network_peer_disconnected(_id: int):
	pass

func _on_server_disconnected():
	pass

func host():
	var host := NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err := host.create_server(DEFAULT_PORT, 2)
	if err != OK:
		_fail(err)
		return

	get_tree().set_network_peer(host)

	emit_signal("host_ok", "great")

func join(address: String):
	var host := NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err := host.create_client(address, DEFAULT_PORT)
	if err:
		_fail(err)
		return
	get_tree().set_network_peer(host)

remotesync func begin():
	var game = preload("res://Game.tscn").instance()
	game.scene = preload("res://arena/godot/Loop.tscn")
	get_tree().root.add_child(game)
	get_tree().current_scene = game
