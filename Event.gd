extends Node

# warning-ignore-all: UNUSED_SIGNAL
signal time_trial_finished()
signal lap_completed(num, time)
signal records_imported()
signal new_record()

signal race_intro_started()
signal race_countdown_started()
signal race_started()

signal quick_fade()
