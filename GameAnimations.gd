extends AnimationPlayer

func _ready():
	Event.connect("time_trial_finished", self, "play", ["Fade"])
	Event.connect("quick_fade", self, "play", ["IntroSkip"])
