extends Node

var _once := {}

func print_once(msg: String):
	var key := hash(get_stack()[1])
	if not key in _once:
		_once[key] = true
		print(msg)

func print_every(frames: int, msg: String):
	if get_tree().get_frame() % frames == 0:
		print(msg)
