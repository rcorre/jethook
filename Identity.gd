extends HTTPRequest

# https://itch.io/docs/api/serverside#reference/profileme-httpsitchioapi1keyme
# Example:
# {
#   "user": {
#     "username": "fasterthanlime",
#     "gamer": true,
#     "display_name": "Amos",
#     "cover_url": "https://img.itch.zone/aW1hZ2UyL3VzZXIvMjk3ODkvNjkwOTAxLnBuZw==/100x100%23/JkrN%2Bv.png",
#     "url": "https://fasterthanlime.itch.io",
#     "press_user": true,
#     "developer": true,
#     "id": 29789
#   }
#   "api_key": {
#   	"expires_at": 1234,
#   	"issuer": {
#   		"game_id": 1234
#   	},
#   	"type": "jwt"
#   }
# }

signal load_start
signal load_ok
signal load_err

var data := {}

func fail(msg: String):
	emit_signal("load_err")
	push_error(msg)

func _ready():
	if not auth():
		return

	var headers := PoolStringArray([auth()])

	connect("request_completed", self, "_response")

	emit_signal("load_start")
	var err := request("https://itch.io/api/1/jwt/me", headers)
	if err != OK:
		fail("Failed to make request: %d" % err)

func auth() -> String:
	if not OS.has_environment("ITCHIO_API_KEY"):
		return ""
	return "Authorization: Bearer %s" % OS.get_environment("ITCHIO_API_KEY")

func _response(result: int, code: int, _headers: PoolStringArray, body: PoolByteArray):
	if result != RESULT_SUCCESS:
		fail("HTTP result %d != %d" % [result, RESULT_SUCCESS])
	if code != HTTPClient.RESPONSE_OK:
		fail("HTTP response %d != %d" % [code, 200])

	var utf8 := body.get_string_from_utf8()
	var response = parse_json(utf8)
	if not response is Dictionary:
		fail("Could not parse: %s" % utf8)
		return

	data = response
	if not "user" in data:
		fail("missing user from: %s" % utf8)
		return
	emit_signal("load_ok")

func name() -> String:
	return data.get("user", {}).get("username", "You")
