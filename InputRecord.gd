extends Object
class_name InputRecord

const FORMAT_VERSION := 2
const GRAPPLE_OFF := Vector3.INF
const FRAME_TIME := 1.0 / Engine.iterations_per_second
const USER_DIR := "user://records"
const BUILTIN_DIR := "res://records"

# Data from an InputRecorder can be persisted to an InputRecord
# and loaded into an InputRepeater to create a "ghost"

class Data:
	var level: int
	var name: String
	var grapple: PoolVector3Array
	var pos: PoolVector3Array
	var jet: PoolVector3Array
	var frames: PoolIntArray
	var time: float

	static func from_dict(d: Dictionary) -> Data:
		if not (
			d.get("UserName") is String and
			d.get("Level") is float and  # really an int, but JSON is all floats
			d.get("Time") is float and
			d.get("Data") is String
		):
			push_error("Ill-formatted record %s" % d)
			return null

		var res := Data.new()
		res.name = d["UserName"]
		res.level = d["Level"]
		res.time = d["Time"]
		var data := Marshalls.base64_to_variant(d["Data"]) as Dictionary
		if not data:
			push_error("Ill-formatted record %s" % d)
			return null
		res.pos = data["Pos"]
		res.grapple = data["Grapple"]
		res.jet = data["Jet"]
		res.frames = data["Frames"]

		return res

	func to_dict() -> Dictionary:
		return {
			"UserName": name,
			"Level": level,
			"Time": time,
			"Data": Marshalls.variant_to_base64({
				"Pos": pos,
				"Grapple": grapple,
				"Jet": jet,
				"Frames": frames,
			})
		}

	static func _read_vec(file: File, size: int) -> PoolVector3Array:
		var ret := PoolVector3Array()
		ret.resize(size)
		for i in range(size):
			ret[i] = Vector3(
				file.get_float(),
				file.get_float(),
				file.get_float()
			)
		return ret

	static func _read_ints(file: File, size: int) -> PoolIntArray:
		var ret := PoolIntArray()
		ret.resize(size)
		for i in range(size):
			ret[i] = file.get_64()
		return ret

	static func read(path: String) -> Data:
		var file := File.new()
		if file.open(path, File.READ) == OK:
			var res := from_dict(parse_json(file.get_as_text()))
			file.close()
			return res
		push_error("Failed to open %s" % path)
		return null

	func write(path: String):
		assert(frames.size() == grapple.size())
		assert(frames.size() == jet.size())
		var file := File.new()
		if file.open(path, File.WRITE) == OK:
			file.store_string(to_json(to_dict()))
			file.close()
		else:
			printerr("failed to open ", path)

	func save() -> bool:
		assert(time != 0)
		Directory.new().make_dir_recursive(USER_DIR)
		if File.new().file_exists(path()) and read(path()).time < time:
			return false
		write(path())
		return true

	func path() -> String:
		return "%s/%s_%d.json" % [USER_DIR, name, level]

	func push(frame: int, grapple_pos: Vector3, jet_dir: Vector3, p: Vector3):
		frames.push_back(frame)
		grapple.push_back(grapple_pos)
		jet.push_back(jet_dir)
		pos.push_back(p)

	func delete():
		Directory.new().remove(path())

static func read(path: String) -> Data:
	return Data.read(path)

class SortByTime:
	static func sort_ascending(a: Data, b: Data):
		return a.time < b.time

static func _load(level: int, dirpath: String) -> Array:
	var res := Array()
	var dir := Directory.new()
	if dir.open(dirpath) == OK:
		dir.list_dir_begin(true, true)
		var filename := dir.get_next()
		while filename != "":
			if filename.ends_with("_%d.json" % level):
				var data := read(dir.get_current_dir().plus_file(filename))
				if data:
					res.push_back(data)
			filename = dir.get_next()
		dir.list_dir_end()
	else:
		printerr("failed to open ", dirpath)
	return res

# returns []Record
static func load(level: int) -> Array:
	var res := _load(level, USER_DIR) + _load(level, BUILTIN_DIR)
	res.sort_custom(SortByTime, "sort_ascending")
	return res
