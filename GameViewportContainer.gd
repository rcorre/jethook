extends Control

onready var viewport: Viewport = find_node("Viewport")

var scene: PackedScene

func _ready():
	viewport.add_child(scene.instance())

func _input(ev: InputEvent):
	viewport.input(ev)
