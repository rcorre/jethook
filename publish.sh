#!/bin/sh

set -e

rm -r build/{linux,windows,osx}
mkdir -p build/{linux,windows,osx}

godot --export linux build/linux/jethook.x86_64

godot --export windows build/windows/jethook.exe

godot --export osx /tmp/jethook-osx-beta.zip
unzip /tmp/jethook-osx-beta.zip -d build/osx/
rm /tmp/jethook-osx-beta.zip

cp itch.toml build/linux/.itch.toml
cp itch.toml build/windows/.itch.toml
cp itch.toml build/osx/.itch.toml

butler push build/linux rcorre/jethook:linux-beta
butler push build/windows rcorre/jethook:windows-beta
butler push build/osx rcorre/jethook:osx-beta
