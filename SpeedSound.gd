extends AudioStreamPlayer3D

export(float, -100, 100, 0.1) var slow_db := -60
export(float, -100, 100, 0.1) var fast_db := -20
export(float, 0, 100, 1.0) var max_speed := 60

var tracker := SpatialVelocityTracker.new()

func _ready():
	unit_db = slow_db

func _process(_delta: float):
	tracker.update_position(global_transform.origin)
	unit_db = lerp(slow_db, fast_db, tracker.get_tracked_linear_velocity().length() / max_speed)
