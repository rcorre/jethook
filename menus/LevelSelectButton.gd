extends BaseButton

signal change_preview(texture)
signal level_selected(level_id)

export(PackedScene) var scene: PackedScene
export(Texture) var preview_texture: Texture
export(int) var level_id: int

var time := ""

func _ready():
	connect("mouse_entered", self, "_on_mouse_entered")
	connect("mouse_exited", self, "_on_mouse_exited")
	Event.connect("records_imported", self, "_on_records_imported")
	_on_records_imported()

func _on_records_imported():
	time = ""

func _pressed():
	Global.load_level(scene)

func _on_mouse_entered():
	emit_signal("change_preview", preview_texture)
	emit_signal("level_selected", level_id)

func _on_mouse_exited():
	emit_signal("change_preview", null)
	emit_signal("level_selected", 0)
