extends BaseButton

export(String) var section: String
export(String) var key: String

func _ready():
	pressed = UserPreferences.get_value(section, key)
	connect("toggled", self, "_on_toggled")

func _on_toggled(on: bool):
	UserPreferences.set_value(section, key, on)
	if has_node("Sound"):
		var sound := $Sound as AudioStreamPlayer
		if sound and not sound.is_playing():
			sound.play()
