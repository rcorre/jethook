extends LineEdit

signal changed

export(String) var section: String
export(String) var key: String

func _ready():
	text = UserPreferences.get_value(section, key)
	connect("text_entered", self, "_on_value_changed")

func _on_value_changed(val: String):
	UserPreferences.set_value(section, key, val)
	emit_signal("changed")
