extends Button

export(int, 0, 10) var tab := 0

func _pressed():
	get_tree().set_group("title_menu", "current_tab", tab)
