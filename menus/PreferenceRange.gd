extends Range

export(String) var section: String
export(String) var key: String

func _ready():
	var bound = UserPreferences.SETTINGS[section][key].bound
	min_value = bound[0]
	max_value = bound[1]
	value = UserPreferences.get_value(section, key)
	connect("value_changed", self, "_on_value_changed")

func _on_value_changed(val: float):
	UserPreferences.set_value(section, key, val)
	if has_node("Sound"):
		var sound := $Sound as AudioStreamPlayer
		if sound and not sound.is_playing():
			sound.play()
