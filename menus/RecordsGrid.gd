extends Control

export(Color) var row_color_even: Color
export(Color) var row_color_odd: Color
export(Color) var player_color: Color

func make_label(text: String, stretch: float, color: Color):
	var label := Label.new()
	label.set_text(text)
	label.size_flags_horizontal = SIZE_EXPAND_FILL
	label.size_flags_stretch_ratio = stretch
	label.clip_text = true
	label.valign = VALIGN_CENTER
	label.modulate = color
	return label

func _on_level_selected(level_id: int):
	for c in get_children():
		remove_child(c)
	if level_id == 0:
		return
	var records := InputRecord.load(level_id)
	for i in len(records):
		var text_color := player_color if Identity.name() == records[i].name else Color.white
		var panel := PanelContainer.new()
		var row := HBoxContainer.new()
		row.add_child(make_label("%-4d" % (i + 1), 0.5, text_color))
		row.add_child(make_label("%s" % records[i].name, 1.5, text_color))
		row.add_child(make_label("%.2f" % records[i].time, 1.0, text_color))
		panel.add_child(row)
		panel.rect_clip_content = true
		var stylebox := StyleBoxFlat.new()
		stylebox.bg_color = row_color_even if i % 2 == 0 else row_color_odd
		stylebox.content_margin_top = 8
		stylebox.content_margin_bottom = 4
		panel.add_stylebox_override("panel", stylebox)
		add_child(panel)
