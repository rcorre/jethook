extends Button

func _ready():
	Identity.connect("load_start", self, "set_text", ["Connecting..."])
	Identity.connect("load_err", self, "set_text", ["Connection Failed"])
	Identity.connect("load_ok", self, "_id_loaded")

func _id_loaded():
	set_text(Identity.name())
	disabled = true
