extends ToolButton

export(String) var link: String

func _pressed():
	OS.shell_open(link)
