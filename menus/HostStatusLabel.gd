extends Label

func _ready():
	set_text("")
	Multiplayer.connect("host_ok", self, "_on_host_ok")
	Multiplayer.connect("host_error", self, "_on_host_error")

func _on_host_ok(address: String):
	set_text("Hosting at: %s" % address)

func _on_host_error(err: String):
	set_text("Hosting failed: %s" % err)
