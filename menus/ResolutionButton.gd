extends OptionButton

func _ready():
	var cur = UserPreferences.get_value("gfx", "resolution")
    # just grabbed these from my system's xrandr output
	for opt in UserPreferences.SETTINGS.gfx.resolution.choices:
		add_item(opt)
		if opt == cur:
			select(get_item_count() - 1)

	connect("item_selected", self, "_on_item_selected")

func _on_item_selected(idx: int):
	UserPreferences.set_value("gfx", "resolution", get_item_text(idx))
