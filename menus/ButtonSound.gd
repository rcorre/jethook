extends AudioStreamPlayer

export(AudioStream) var hover_sound: AudioStream
export(AudioStream) var click_sound: AudioStream

onready var button := get_parent() as BaseButton

func _ready():
	button.connect("focus_entered", self, "play_sound", [hover_sound])
	button.connect("mouse_entered", self, "play_sound", [hover_sound])
	button.connect("pressed", self, "play_sound", [click_sound])

func play_sound(sound: AudioStream):
	if not button.disabled:
		stream = sound
		play()
