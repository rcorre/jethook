extends Spatial

const HEAT_PER_JET := 0.5

signal jet(dir)

onready var body: Spatial = owner
onready var sound: AudioStreamPlayer3D = $Sound

var tracker := SpatialVelocityTracker.new()
var heat := 0.0

func _physics_process(delta: float):
	tracker.update_position(global_transform.origin)
	heat = max(heat - delta, 0)

func _on_jet(dir: Vector2):
	if heat >= 1.0:
		return

	heat += HEAT_PER_JET

	emit_signal("jet", global_transform.basis.xform(Vector3(dir.x, dir.y, 0)))

	sound.stop()
	sound.play()
