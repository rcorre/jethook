extends RayCast

const GRAPPLE_FORCE := 48.0
const GRAPPLE_RANGE := 55

signal grapple_on(target, offset)
signal grapple_off()

onready var sound := $Sound as AudioStreamPlayer3D

var pos: Vector3
var on := false

func _physics_process(delta: float):
	if on:
		var dir = owner.global_transform.origin.direction_to(pos)
		owner.linear_velocity += dir.normalized() * GRAPPLE_FORCE * delta

func hook(global_pos: Vector3):
	emit_signal("grapple_on", global_pos)
	pos = global_pos
	on = true
	sound.playing = true

func release():
	emit_signal("grapple_off")
	on = false
	sound.playing = false
