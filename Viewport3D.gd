extends Viewport

func _ready():
	var xy = UserPreferences.get_value("gfx", "resolution").split("x")
	if len(xy) != 2:
		xy = [1920, 1080]
	set_size(Vector2(xy[0], xy[1]))
	_evaluate_usage()
	UserPreferences.connect("changed", self, "_evaluate_usage")

func _evaluate_usage(_section := "", _key := "", _val = null):
	usage = USAGE_3D if (
		UserPreferences.get_value("gfx", "motion_blur") or
		UserPreferences.get_value("gfx", "ss_reflections")
	) else USAGE_3D_NO_EFFECTS
