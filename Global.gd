extends Node

const VERSION_MAJOR := 0
const VERSION_MINOR := 2
const VERSION_PATCH := 0

enum PhysicsLayer {
	DEFAULT = 1,
	GRABBABLE = 2,
	THROWN = 4
	PLAYER = 8
}

var _current_scene: PackedScene

func level() -> Level:
	var levels = get_tree().get_nodes_in_group("level")
	# might be 2 if we're restarting, but one will be queue_free'ing
	for l in levels:
		if is_instance_valid(l):
			return l
	assert(false, "No valid level")
	return null

func player() -> Player:
	var players = get_tree().get_nodes_in_group("player")
	assert(players.size() == 1)
	return players[0]

func load_level(scene: PackedScene):
	_current_scene = scene
	get_tree().current_scene.queue_free()
	var game = preload("res://Game.tscn").instance()
	game.scene = scene
	get_tree().root.add_child(game)
	get_tree().current_scene = game

func reload_level():
	load_level(_current_scene)
