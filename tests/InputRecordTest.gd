extends Node

func _ready():
	test_persist()
	print("test_persist passed!")
	get_tree().quit()

func assert_eq(actual, expected):
	assert(actual == expected, "%s != %s" % [actual, expected])

func make_record(name: String, level: String) -> InputRecord.Data:
	var size := randi() % 50 + 1
	var d := InputRecord.Data.new()
	for _i in range(size):
		d.grapple.push_back(Vector3(randf(), randf(), randf()))
		d.jet.push_back(Vector3(randf(), randf(), randf()))
		d.frames.push_back(randi())

	d.name = name
	d.level = level
	d.time = randf() * 1000

	return d

func test_persist():
	var rec1 := make_record("name1", "level1")
	var rec2 := make_record("name2", "level2")
	rec1.save()
	rec2.save()

	var act1 := InputRecord.load("level1")
	assert_eq(act1.size(), 1)
	assert(is_equal_approx(act1[0].time, rec1.time))
	assert_eq(act1[0].name, "name1")
	assert_eq(act1[0].level, rec1.level)
	assert_eq(act1[0].grapple, rec1.grapple)
	assert_eq(act1[0].jet, rec1.jet)
	assert_eq(act1[0].frames, rec1.frames)

	var act2 := InputRecord.load("level2")
	assert_eq(act2.size(), 1)
	assert(is_equal_approx(act2[0].time, rec2.time))
	assert_eq(act2[0].name, "name2")
	assert_eq(act2[0].level, rec2.level)
	assert_eq(act2[0].grapple, rec2.grapple)
	assert_eq(act2[0].jet, rec2.jet)
	assert_eq(act2[0].frames, rec2.frames)
