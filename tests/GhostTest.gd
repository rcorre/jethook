extends Spatial

export(PackedScene) var ghost_scene

onready var player := $Player as Player

var spawn_trans: Transform

func _ready():
	spawn_trans = player.global_transform
	player.activate()

func _unhandled_input(ev: InputEvent):
	var key := ev as InputEventKey
	if key and key.pressed and key.scancode == KEY_E:
		var ghost = player.create_ghost()
		ghost.find_node("Camera").call("focus_camera")
