extends Spatial
class_name InputRecorder

# Records a player's input
# Used to create "ghost" characters

var data := InputRecord.Data.new()
var grapple_pos := InputRecord.GRAPPLE_OFF
var jet_dir := Vector3.ZERO
var start_frame := 0
var changed := false

func _ready():
	data.level = Global.level().level_id
	data.name = Identity.name()
	Event.connect("time_trial_finished", self, "_finish")

func _physics_process(delta: float):
	if start_frame == 0:
		start_frame = get_tree().get_frame()
		data.push(0, InputRecord.GRAPPLE_OFF, Vector3.ZERO, global_transform.origin)
	if changed:
		changed = false
		var frame := get_tree().get_frame() - start_frame
		data.push(frame, grapple_pos, jet_dir, global_transform.origin)
	jet_dir = Vector3.ZERO
	data.time += delta

func _on_grapple_on(target: Vector3):
	grapple_pos = target
	changed = true

func _on_grapple_off():
	grapple_pos = InputRecord.GRAPPLE_OFF
	changed = true

func _on_jet(dir: Vector3):
	jet_dir = dir
	changed = true

func _finish():
	var frame := get_tree().get_frame() - start_frame
	data.push(frame, InputRecord.GRAPPLE_OFF, Vector3.ZERO, global_transform.origin)
	if data.save():
		Event.emit_signal("new_record")
		Leaderboard.upload(data)
