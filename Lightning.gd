extends ImmediateGeometry

export(float, 0, 10, 0.1) var jitter := 0.4

var target := Vector3.INF

func _on_grapple_on(pos: Vector3):
	target = pos

func _on_grapple_off():
	target = Vector3.INF

func _process(_delta: float):
	clear()
	if target == Vector3.INF:
		return
	var step := to_local(target) / 10
	for _i in range(0, 6):
		begin(Mesh.PRIMITIVE_LINE_STRIP)
		for i in range(0, 10):
			var p := step * i + Vector3(randf(), randf(), randf()) * jitter * i / 10.0
			add_vertex(p)
		end()
