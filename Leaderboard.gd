extends Node

const URL := "https://jethook.herokuapp.com/v1/records"
const SSL := true

signal error
signal upload_complete
signal download_complete

var enabled := false
var upload_request := HTTPRequest.new()
var download_request := HTTPRequest.new()

func _ready():
	enabled = OS.has_environment("ITCHIO_API_KEY")
	add_child(upload_request)
	add_child(download_request)
	upload_request.connect("request_completed", self, "_on_upload_completed")
	download_request.connect("request_completed", self, "_on_download_completed")

	download()

func fail(msg: String):
	emit_signal("error")
	push_error(msg)

func headers() -> PoolStringArray:
	var token := OS.get_environment("ITCHIO_API_KEY")
	return PoolStringArray(["Authorization: Bearer %s" % token])

func upload(data: InputRecord.Data):
	if not enabled:
		print_debug("Skipping upload, no auth")
		return

	var s := to_json(data.to_dict())
	var err := upload_request.request(URL, headers(), SSL, HTTPClient.METHOD_POST, s)
	if err != OK:
		fail("Failed to make request: %d" % err)
	print_debug("Upload initiated")

func download():
	if not enabled:
		print_debug("Skipping download, no auth")
		return

	var err := download_request.request(URL, headers(), SSL, HTTPClient.METHOD_GET)
	if err != OK:
		fail("Failed to make request: %d" % err)
	print_debug("Download initiated")

func check_result(result: int, code: int) -> bool:
	if result != HTTPRequest.RESULT_SUCCESS:
		fail("HTTP result %d != %d" % [result, HTTPRequest.RESULT_SUCCESS])
		return false
	if code != 200:
		fail("HTTP response %d != %d" % [code, 200])
		return false
	return true

func _on_upload_completed(result: int, code: int, _headers: PoolStringArray, _body: PoolByteArray):
	check_result(result, code)
	print_debug("Upload complete")
	emit_signal("upload_complete")

func _on_download_completed(result: int, code: int, _headers: PoolStringArray, body: PoolByteArray):
	if !check_result(result, code):
		return

	var utf8 := body.get_string_from_utf8()
	var records = parse_json(utf8)
	if not records is Array:
		fail("Expected array, got: %s" % utf8)
		return

	for r in records:
		if not r is Dictionary:
			fail("Expected dict, got: %s" % r)
			continue
		var d := InputRecord.Data.from_dict(r)
		d.save()

	emit_signal("download_complete")
	print_debug("Upload complete")
