extends Control

export(NodePath) var node_path: NodePath

func _ready():
	connect("visibility_changed", self, "_on_visibility_changed")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	AudioServer.set_bus_layout(preload("res://default_bus_layout.tres"))

func _on_visibility_changed():
	var node := get_node(node_path) as Control
	if visible and node:
		node.grab_focus()
