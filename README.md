# JetHook

Use your grappling hook to hurtle through the air and complete the course in record time!
Move the mouse to aim and click to grapple. That's all there is to it!

This is an entry for [OpenJam 2020](https://itch.io/jam/open-jam-2020).

Make sure to set up [git-lfs](https://git-lfs.github.com/) before cloning, or the assets will be missing.

# License

This game contains assets from many authors. See [Credits](#credits) for details on the assets and their licenses.

Assets not mentioned in [Credits](#credits) were created by me, and are licensed as follows:

- Code is [MIT](https://opensource.org/licenses/MIT) licensed
- Assets are [CC-BY-SA-4.0](https://creativecommons.org/licenses/by/4.0/legalcode) licensed

# Credits

## Code

- [Motion Blur](https://github.com/Bauxitedev/godot-motion-blur) shader by @Bauxitedev (MIT)

## Music 

- [Arturia Acid](https://freesound.org/people/Doctor_Dreamchip/sounds/458557) by Doctor Dreamchip (CC0)
- [2018-08-10](https://freesound.org/people/Doctor_Dreamchip/sounds/457885) by Doctor Dreamchip (CC0)
- [2020-06-11](https://freesound.org/people/Doctor_Dreamchip/sounds/522945) by Doctor Dreamchip (CC0)
- [2020-04-27](https://freesound.org/people/Doctor_Dreamchip/sounds/523808/) by Doctor Dreamchip (CC0)

## Sounds
- [Applause](https://freesound.org/people/Bansemer/sounds/160493/) sound by Bansemer (CC0)
- [Wind](https://freesound.org/people/LolitaPerdurabo/sounds/208707/) sound by LolitaPerdurabo (CC-BY-3.0)
- [Boost](https://freesound.org/people/TiesWijnen/sounds/413312/) sound by TiesWijnen (CC0)
- [Menu Select](https://freesound.org/people/LittleRobotSoundFactory/sounds/288965/) sound by LittleRobotSoundFactory (CC-BY-3.0)
- [Menu Change](https://freesound.org/people/victorium183/sounds/476816/) sound by victorium183 (CC0)
- [Fountain](https://freesound.org/people/victorium183/sounds/476816/) sound by skyko (CC0)
- Lap Complete sound created with [bfxr](https://www.bfxr.net/?s=2%2C0.5%2C%2C0.0162%2C0.3988%2C0.3888%2C0.3%2C0.7874%2C%2C%2C%2C%2C%2C%2C%2C%2C0.4444%2C0.6506%2C%2C%2C%2C%2C%2C%2C%2C1%2C%2C%2C%2C%2C%2C%2CmasterVolume)
