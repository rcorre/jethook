extends KinematicBody
class_name Player

const MOUSE_SENSITIVITY := 0.005
const JOY_SENSITIVITY := 3.0
const POS_CORRECTION_FACTOR := 0.1
const GRAVITY := 9.8
const DAMPING := 0.2
const JET_SPEED := 10.0
const JET_SLOW_FACTOR := 0.1

# warning-ignore-all: unused_signal

# emitted to request a new rotational orientation
# amount_rad: Vector2(x_axis_rotation, y_axis_rotation) in radians
signal look(amount_rad)

# emitted to engage or release the grappling hook
# on: true to engage, false to release
signal grapple(on)

# emitted to boost the jetpack in a direction
# dir: Vector2 direction in the x/y plane
signal jet(dir)

onready var recorder: InputRecorder = $InputRecorder

var linear_velocity := Vector3.ZERO
var look_dir := Vector2.ZERO
puppet var puppet_pos: Vector3

func _ready():
	Event.connect("time_trial_finished", self, "deactivate")
	deactivate()

func deactivate():
	hide()
	propagate_call("set_process_input", [false])
	propagate_call("set_process", [false])
	propagate_call("set_physics_process", [false])

func activate():
	puppet_pos = global_transform.origin
	show()
	propagate_call("set_process_input", [true])
	propagate_call("set_process", [true])
	propagate_call("set_physics_process", [true])

func focus_camera():
	find_node("Camera").call("focus_camera")

func _on_body_entered(body: Node):
	body.propagate_call("_on_player_collided")

func _on_reset():
	var paths := get_tree().get_nodes_in_group("main_path")
	if len(paths) != 1:
		printerr("Expected 1 main_path, got %s", paths)
		return

	# warning-ignore: unsafe_cast
	var path := paths[0] as Path
	if not path is Path:
		printerr("main_path not a path: %s", paths[0])
		return

	linear_velocity = Vector3.ZERO
	global_transform.origin = path.to_global(
		path.curve.get_closest_point(
			path.to_local(global_transform.origin)
		)
	)
	Event.emit_signal("quick_fade")

func _input(ev: InputEvent):
	if not is_local():
		return
	var mouse := ev as InputEventMouseMotion
	if mouse:
		var motion := mouse.relative * MOUSE_SENSITIVITY
		look_dir.y -= motion.x
		look_dir.x = clamp(look_dir.x - motion.y, -PI/2, PI/2)
		emit_sync(["look", look_dir])
	elif ev.is_action("grapple"):
		emit_sync(["grapple", ev.is_pressed()])
	elif ev.is_action_pressed("up"):
		emit_sync(["jet", Vector2(0, 1)])
	elif ev.is_action_pressed("down"):
		emit_sync(["jet", Vector2(0, -1)])
	elif ev.is_action_pressed("left"):
		emit_sync(["jet", Vector2(-1, 0)])
	elif ev.is_action_pressed("right"):
		emit_sync(["jet", Vector2(1, 0)])
	elif ev.is_action_pressed("reset"):
		# TODO: sync
		_on_reset()

func _physics_process(delta: float):
	var motion := Vector2(
		Input.get_action_strength("look_right") - Input.get_action_strength("look_left"),
		Input.get_action_strength("look_down") - Input.get_action_strength("look_up")
	) * delta * JOY_SENSITIVITY

	if motion != Vector2.ZERO:
		look_dir.y -= motion.x
		look_dir.x = clamp(look_dir.x - motion.y, -PI/2, PI/2)
		emit_sync(["look", look_dir])

	if is_master():
		rset_unreliable("puppet_pos", global_transform.origin)
	elif is_puppet():
		# rubber band to where the network master says our position is
		global_transform.origin = global_transform.origin.linear_interpolate(puppet_pos, delta)

	linear_velocity.y -= GRAVITY * delta
	linear_velocity.x *= (1 - DAMPING * delta)
	linear_velocity.z *= (1 - DAMPING * delta)
	linear_velocity = move_and_slide(linear_velocity)

func emit_sync(args: Array):
	if get_tree().network_peer == null:
		_do_emit_sync(args)
	else:
		rpc("_do_emit_sync", args)

remotesync func _do_emit_sync(args: Array):
	callv("emit_signal", args)

func is_local():
	return get_tree().network_peer == null or int(name) == get_tree().get_network_unique_id()

func is_master():
	return get_tree().network_peer != null and is_network_master()

func is_puppet():
	return get_tree().network_peer != null and not is_network_master()

func _apply_jet(dir: Vector3):
	linear_velocity *= (1 - JET_SLOW_FACTOR)
	linear_velocity += dir * JET_SPEED
